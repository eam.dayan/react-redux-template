import { createStore } from "redux";
import { applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { articleReducer } from "../reducers/articleReducer";

export const store = createStore(articleReducer, applyMiddleware(thunk));
