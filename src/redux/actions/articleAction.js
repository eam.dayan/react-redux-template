import * as api from "../../services/article.service";

export const FUNCTION = "FUNCTION";

export const fetchAllArticles = () => {
  return async (dispatch) => {
    const articles = await api.fetchAll();
    dispatch({
      type: FUNCTION,
      payload: articles,
    });
  };
};
