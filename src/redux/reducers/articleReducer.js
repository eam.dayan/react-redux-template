import { FUNCTION } from "../actions/articleAction";

const initialState = {
  sth: [],
};

export const articleReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FUNCTION:
      return { ...state, ...payload };

    default:
      return state;
  }
};
