import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./views/Home";
import Navigation from "./components/Navigation";

function App() {
  return (
    <div className='App'>
      <BrowserRouter>
        <Navigation />
        <Switch>
          <Route exact to='/'>
            <Home />
          </Route>
          <Route to='/'></Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
