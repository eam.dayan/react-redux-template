#!/bin/bash

directories=(services redux redux/actions redux/reducers redux/store views components)
for file in ${directories[@]};
do 
    mkdir $file
done


NAME=article
directories=(
    redux/actions/"$NAME"Action.js
    redux/reducers/"$NAME"Reducer.js
    redux/store/store.js
    services/API.js
    services/"$NAME".service.js
    views/Home.jsx
    components/Navigation.jsx
)
for file in ${directories[@]};
do 
    touch $file
done